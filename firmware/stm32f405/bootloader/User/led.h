#ifndef __LED_H
#define __LED_H
//PD7
#define LED_RUN_PORT  GPIOD
#define LED_RUN_PIN   GPIO_PIN_7
//PD13
#define LED_BOOT_PORT  GPIOD
#define LED_BOOT_PIN   GPIO_PIN_13

#define LED_BUSY1_PORT  GPIOD
#define LED_BUSY1_PIN   GPIO_PIN_14

#define LED_BUSY2_PORT  GPIOD
#define LED_BUSY2_PIN   GPIO_PIN_15


void MX_LED_Init(void);
void LED_RUN_On(void);
void LED_RUN_Off(void);
void LED_BOOT_On(void);
void LED_BOOT_Off(void);
void LED_BUSY1_On(void);
void LED_BUSY1_Off(void);
void LED_BUSY2_On(void);
void LED_BUSY2_Off(void);

void LED_LINBusyOn(uint8_t Ch);
void LED_LINBusyOff(uint8_t Ch);
void LED_CANBusyOn(uint8_t Ch);
void LED_CANBusyOff(uint8_t Ch);
#endif
