#ifndef __LIN_BUFFER_H
#define __LIN_BUFFER_H

#include <stdint.h>
#include "lin_driver.h"

//定义数据缓冲区
typedef struct _LIN_BUFFER{
  uint8_t  WriteIndex;  //当前正在接收数据的帧索引
  uint8_t  ReadIndex;   //完整帧，读数据起始索引
  uint8_t  GotMsgNum;   //接收到的完整帧数，这些帧可以被上位机读取走
  uint8_t  BufferSize;  //缓冲区能装下的最大帧数
  LIN_EX_MSG *pLINBuffer;//缓冲区首地址
}LIN_BUFFER;

void LIN_BUF_Init(LIN_BUFFER *pLINBuf,LIN_EX_MSG *pRxBuf,int BufSize);
LIN_EX_MSG *LIN_BUF_GetRxingMsg(LIN_BUFFER *pLINBuf);
void LIN_BUF_ApendMsg(LIN_BUFFER *pLINBuf);
int LIN_BUF_GetAllRxedMsg(LIN_BUFFER *pLINBuf,LIN_EX_MSG **pMsg);
int LIN_BUF_GetRxedMsgWithSize(LIN_BUFFER *pLINBuf,LIN_EX_MSG **pMsg,int Size);
void LIN_BUF_ClearRxingMsg(LIN_BUFFER *pLINBuf);
uint8_t LIN_BUF_GetRxingMsgDlc(LIN_BUFFER *pLINBuf);

#endif
