#include "main.h"
#include "lin_buffer.h"


//初始化缓冲区
void LIN_BUF_Init(LIN_BUFFER *pLINBuf,LIN_EX_MSG *pRxBuf,int BufSize)
{
  pLINBuf->BufferSize = BufSize;
  pLINBuf->pLINBuffer = pRxBuf;
  pLINBuf->ReadIndex = 0;
  pLINBuf->WriteIndex = 0;
  pLINBuf->GotMsgNum = 0;
}

//获取当前正在接收数据帧
LIN_EX_MSG *LIN_BUF_GetRxingMsg(LIN_BUFFER *pLINBuf)
{
  return &pLINBuf->pLINBuffer[pLINBuf->WriteIndex];
}

//表示成功读取到一帧数据
void LIN_BUF_ApendMsg(LIN_BUFFER *pLINBuf)
{
  pLINBuf->WriteIndex = (++pLINBuf->WriteIndex)&(pLINBuf->BufferSize-1);//写缓冲区递增
  //判断缓冲区是否已满
  if(pLINBuf->WriteIndex==pLINBuf->ReadIndex){
    pLINBuf->ReadIndex = (++pLINBuf->ReadIndex)&(pLINBuf->BufferSize-1);//读缓冲区递增
  }else{
    pLINBuf->GotMsgNum++; //有效数据加1
  }
}

//获取成功读到的消息
int LIN_BUF_GetAllRxedMsg(LIN_BUFFER *pLINBuf,LIN_EX_MSG **pMsg)
{
  __set_PRIMASK(1);
  int MsgNum = ((pLINBuf->WriteIndex>=pLINBuf->ReadIndex)?pLINBuf->GotMsgNum:(pLINBuf->BufferSize-pLINBuf->ReadIndex));
  *pMsg = &pLINBuf->pLINBuffer[pLINBuf->ReadIndex];
  pLINBuf->GotMsgNum -= MsgNum;
  pLINBuf->ReadIndex = (pLINBuf->ReadIndex+MsgNum)&(pLINBuf->BufferSize-1);
  __set_PRIMASK(0);
  return MsgNum;
}

int LIN_BUF_GetRxedMsgWithSize(LIN_BUFFER *pLINBuf,LIN_EX_MSG **pMsg,int Size)
{
  __set_PRIMASK(1);
  int MsgNum = ((pLINBuf->WriteIndex>=pLINBuf->ReadIndex)?pLINBuf->GotMsgNum:(pLINBuf->BufferSize-pLINBuf->ReadIndex));
  *pMsg = &pLINBuf->pLINBuffer[pLINBuf->ReadIndex];
  MsgNum = ((Size>MsgNum)?MsgNum:Size);
  pLINBuf->GotMsgNum -= MsgNum;
  pLINBuf->ReadIndex = (pLINBuf->ReadIndex+MsgNum)&(pLINBuf->BufferSize-1);
  __set_PRIMASK(0);
  return MsgNum;
}

//清空当前接收帧数据
void LIN_BUF_ClearRxingMsg(LIN_BUFFER *pLINBuf)
{
  LIN_EX_MSG *pMsg=LIN_BUF_GetRxingMsg(pLINBuf);
  //pMsg->MsgType = 0;
  pMsg->DataLen = 0;
  pMsg->Sync = 0;
  pMsg->PID = 0;
  pMsg->Check = 0;
}

//获取当前接收帧已经接收到的数据字节数
uint8_t LIN_BUF_GetRxingMsgDlc(LIN_BUFFER *pLINBuf)
{
  LIN_EX_MSG *pMsg=LIN_BUF_GetRxingMsg(pLINBuf);
  return pMsg->DataLen;
}
