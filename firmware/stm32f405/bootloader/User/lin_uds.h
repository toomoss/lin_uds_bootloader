#ifndef __LIN_UDS_H
#define __LIN_UDS_H

#define MAX_LIN_CH        1
#define LIN_BUFFER_SIZE   64
#define UDS_PACK_SIZE   1024//建议为4字节整数倍

void LIN_UDS_Process(LIN_BUFFER *pLINRxBuf);
void LIN_UDS_Respond(uint8_t RSID,uint8_t *pData,uint16_t DataLen);


#endif
