#include "main.h"
#include "debug.h"
#include "led.h"
#include "lin_driver.h"
#include "lin_buffer.h"

#if 1
#define USART1_DMA_TX_IRQHandler        DMA2_Stream7_IRQHandler

USART_TypeDef*            lin_uart[MAX_LIN_CH]={USART1};
UART_HandleTypeDef        lin_huart[MAX_LIN_CH];
const IRQn_Type           lin_irq[MAX_LIN_CH]={USART1_IRQn};
GPIO_TypeDef*             lin_men_port[MAX_LIN_CH]={GPIOC};
const uint32_t            lin_men_pin[MAX_LIN_CH]={GPIO_PIN_6};
GPIO_TypeDef*             lin_vbat12_port[MAX_LIN_CH]={GPIOC};
const uint32_t            lin_vbat12_pin[MAX_LIN_CH]={GPIO_PIN_13};
GPIO_TypeDef*             lin_vbat5_port[MAX_LIN_CH]={GPIOD};
const uint32_t            lin_vbat5_pin[MAX_LIN_CH]={GPIO_PIN_2};
GPIO_TypeDef*             pwm_port[MAX_LIN_CH]={GPIOB};
const uint32_t            pwm_pin[MAX_LIN_CH]={GPIO_PIN_8};
//DMA发送相关
const IRQn_Type           lin_dmatx_irq[MAX_LIN_CH]={DMA2_Stream7_IRQn};
DMA_Stream_TypeDef*       lin_dmatx_stream[MAX_LIN_CH]={DMA2_Stream7};
DMA_HandleTypeDef         lin_dmatx_handle[MAX_LIN_CH];
//DMA接收相关
const IRQn_Type           lin_dmarx_irq[MAX_LIN_CH]={DMA2_Stream5_IRQn};
DMA_Stream_TypeDef*       lin_dmarx_stream[MAX_LIN_CH]={DMA2_Stream5};
DMA_HandleTypeDef         lin_dmarx_handle[MAX_LIN_CH];
TIM_HandleTypeDef         TimeStampTimHandle;
//uint8_t                   lin_uart_buffer[MAX_LIN_CH];
LIN_CONFIG                LINConfig[MAX_LIN_CH];
LIN_EX_MSG                LINRxDataBuf[MAX_LIN_CH][LIN_BUFFER_SIZE];
LIN_EX_MSG                LINTxDataBuf[MAX_LIN_CH][LIN_BUFFER_SIZE];
//uint8_t*                  pUARTReadBufAddr[MAX_LIN_CH]={LINRxDataBuf[0]};
__IO uint32_t             LINTimeStamp = 0;
__IO LIN_STATE            LinRxState[MAX_LIN_CH] = {IDLE};
__IO int                  LIN_RxTimeOut[MAX_LIN_CH]; //LIN接收数据超时

LIN_BUFFER    LINRxBuffer[MAX_LIN_CH]=
{
  {
    .BufferSize = LIN_BUFFER_SIZE,
    .pLINBuffer = (LIN_EX_MSG*)LINRxDataBuf[0],
    .ReadIndex = 0,
    .WriteIndex = 0,
    .GotMsgNum = 0
  }
};
LIN_BUFFER    LINTxBuffer[MAX_LIN_CH]=
{
  {
    .BufferSize = LIN_BUFFER_SIZE,
    .pLINBuffer = (LIN_EX_MSG*)LINTxDataBuf[0],
    .ReadIndex = 0,
    .WriteIndex = 0,
    .GotMsgNum = 0
  }
};
//主从使能控制引脚初始化
void LIN_MEN_Init(int Channel,uint8_t Master)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  __HAL_RCC_GPIOD_CLK_ENABLE();
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  //初始化主从机控制引脚
  GPIO_InitStruct.Pin = lin_men_pin[Channel];
  HAL_GPIO_Init(lin_men_port[Channel], &GPIO_InitStruct);
  if(Master){
    HAL_GPIO_WritePin(lin_men_port[Channel], lin_men_pin[Channel], GPIO_PIN_SET);
  }else{
    HAL_GPIO_WritePin(lin_men_port[Channel], lin_men_pin[Channel], GPIO_PIN_RESET);
  }
}
//控制VBAT引脚输出对应电压
void LIN_VBAT_PowerOut(int Channel,uint8_t out_power)
{
  switch(out_power)
  {
    case VBAT_0V:
      HAL_GPIO_WritePin(lin_vbat5_port[Channel], lin_vbat5_pin[Channel], GPIO_PIN_SET);
      HAL_GPIO_WritePin(lin_vbat12_port[Channel], lin_vbat12_pin[Channel], GPIO_PIN_RESET);
      break;
    case VBAT_12V:
      HAL_GPIO_WritePin(lin_vbat5_port[Channel], lin_vbat5_pin[Channel], GPIO_PIN_SET);
      HAL_GPIO_WritePin(lin_vbat12_port[Channel], lin_vbat12_pin[Channel], GPIO_PIN_SET);
      break;
    case VBAT_5V:
      HAL_GPIO_WritePin(lin_vbat5_port[Channel], lin_vbat5_pin[Channel], GPIO_PIN_RESET);
      HAL_GPIO_WritePin(lin_vbat12_port[Channel], lin_vbat12_pin[Channel], GPIO_PIN_RESET);
      break;
    default:
      break;
  }
}

//VBAT电压输出控制引脚初始化
void LIN_VBAT_Init(int Channel,uint8_t out_power)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  __HAL_RCC_GPIOE_CLK_ENABLE();
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Pin = lin_vbat12_pin[Channel];
  HAL_GPIO_Init(lin_vbat12_port[Channel], &GPIO_InitStruct);
  
  GPIO_InitStruct.Pin = lin_vbat5_pin[Channel];
  HAL_GPIO_Init(lin_vbat5_port[Channel], &GPIO_InitStruct);
  LIN_VBAT_PowerOut(Channel,out_power);
}

//PWM输出控制引脚初始化
void LIN_PWM_CTL_Init(int Channel)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Pin = pwm_pin[Channel];
  HAL_GPIO_Init(pwm_port[Channel], &GPIO_InitStruct);
}

////LIN数据传输引脚初始化
//void LIN_GPIO_Init(int Channel)
//{
//  USART_TypeDef* uart[4]={UART4,UART7,USART1,USART2};
//  for(int i=0;i<MAX_LIN_CH;i++){
//    lin_huart[i].Instance = uart[i];
//    HAL_UART_MspInit(&lin_huart[i]);
//  }
//}

void LIN_UART_Init(int Channel,int BaudRate)
{
  lin_huart[Channel].Instance = lin_uart[Channel];
  lin_huart[Channel].Init.BaudRate = BaudRate;
  lin_huart[Channel].Init.WordLength = UART_WORDLENGTH_8B;
  lin_huart[Channel].Init.StopBits = UART_STOPBITS_1;
  lin_huart[Channel].Init.Parity = UART_PARITY_NONE;
  lin_huart[Channel].Init.Mode = UART_MODE_TX_RX;
  lin_huart[Channel].Init.HwFlowCtl = UART_HWCONTROL_NONE;
  lin_huart[Channel].Init.OverSampling = UART_OVERSAMPLING_16;
  //lin_huart[Channel].TxCpltCallback = HAL_UART_TxHalfCpltCallback;
  HAL_UART_DeInit(&lin_huart[Channel]);
  HAL_LIN_Init(&lin_huart[Channel],UART_LINBREAKDETECTLENGTH_10B);
  //HAL_UART_Init(&lin_huart[Channel]);
  //HAL_UART_Receive_IT(&lin_huart[Channel], &lin_uart_buffer[Channel], 1);
}

void LIN_IT_Init(uint8_t Channel)
{
  READ_REG(lin_uart[Channel]->SR);
  READ_REG(lin_uart[Channel]->CR1);
  READ_REG(lin_uart[Channel]->CR3);
  __HAL_UART_CLEAR_FLAG(&lin_huart[Channel], 0xFFFF);//清除所有错误标志
  __HAL_UART_ENABLE_IT(&lin_huart[Channel],UART_IT_LBD);//使能LIN Break中断
  __HAL_UART_ENABLE_IT(&lin_huart[Channel],UART_IT_RXNE);//使能RXNE中断
  __HAL_UART_DISABLE_IT(&lin_huart[Channel],UART_IT_IDLE);//使能IDLE中断
  HAL_NVIC_SetPriority(lin_irq[Channel], 0, 0);
  HAL_NVIC_EnableIRQ(lin_irq[Channel]);
  HAL_NVIC_SetPriority(lin_dmatx_irq[Channel], 0, 0);
  HAL_NVIC_EnableIRQ(lin_dmatx_irq[Channel]);
}

void LIN_DMA_TX_Init(uint8_t Channel)
{
  lin_dmatx_handle[Channel].Instance = lin_dmatx_stream[Channel];
  lin_dmatx_handle[Channel].Init.Direction = DMA_MEMORY_TO_PERIPH;
  lin_dmatx_handle[Channel].Init.PeriphInc = DMA_PINC_DISABLE;
  lin_dmatx_handle[Channel].Init.MemInc = DMA_MINC_ENABLE;
  lin_dmatx_handle[Channel].Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  lin_dmatx_handle[Channel].Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
  lin_dmatx_handle[Channel].Init.Mode = DMA_NORMAL;
  lin_dmatx_handle[Channel].Init.Priority = DMA_PRIORITY_LOW;
  lin_dmatx_handle[Channel].Init.FIFOMode = DMA_FIFOMODE_DISABLE;
  lin_dmatx_handle[Channel].Init.Channel = DMA_CHANNEL_4;
  //lin_dmatx_handle[Channel].Parent = &lin_huart[Channel];
  HAL_DMA_DeInit(&lin_dmatx_handle[Channel]);
  if(HAL_DMA_Init(&lin_dmatx_handle[Channel]) != HAL_OK)
  {
    Error_Handler();
  }
  __HAL_LINKDMA(&lin_huart[Channel],hdmatx,lin_dmatx_handle[Channel]);
}

void LIN_DMA_RX_Init(uint8_t Channel)
{
  lin_dmarx_handle[Channel].Instance = lin_dmarx_stream[Channel];
  lin_dmarx_handle[Channel].Init.Direction = DMA_PERIPH_TO_MEMORY;
  lin_dmarx_handle[Channel].Init.PeriphInc = DMA_PINC_DISABLE;
  lin_dmarx_handle[Channel].Init.MemInc = DMA_MINC_ENABLE;
  lin_dmarx_handle[Channel].Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  lin_dmarx_handle[Channel].Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
  lin_dmarx_handle[Channel].Init.Mode = DMA_CIRCULAR;
  lin_dmarx_handle[Channel].Init.Priority = DMA_PRIORITY_LOW;
  lin_dmarx_handle[Channel].Init.FIFOMode = DMA_FIFOMODE_DISABLE;
  lin_dmarx_handle[Channel].Init.Channel = DMA_CHANNEL_4;
  //pUARTReadBufAddr[Channel]=LINRxDataBuf[Channel];
  HAL_DMA_DeInit(&lin_dmarx_handle[Channel]);
  if(HAL_DMA_Init(&lin_dmarx_handle[Channel]) != HAL_OK)
  {
    Error_Handler();
  }
  __HAL_LINKDMA(&lin_huart[Channel],hdmarx,lin_dmarx_handle[Channel]);
  //HAL_DMA_Start(&lin_dmarx_handle[Channel],(uint32_t)&lin_uart[Channel]->RDR,(uint32_t)LIN_RxBuffer[Channel],LIN_BUFFER_SIZE*2);
  //SET_BIT(lin_uart[Channel]->CR3, USART_CR3_DMAR);
}

void LIN_EX_SetPPRState(uint8_t Channel,uint8_t state)
{
  if(state){
    HAL_GPIO_WritePin(lin_men_port[Channel], lin_men_pin[Channel], GPIO_PIN_SET);
  }else{
    HAL_GPIO_WritePin(lin_men_port[Channel], lin_men_pin[Channel], GPIO_PIN_RESET);
  }
}

void LIN_HardwareConfig(uint8_t Channel)
{
  //PWM输出控制引脚初始化
  LIN_PWM_CTL_Init(Channel);
  //主从使能控制引脚初始化
  LIN_MEN_Init(Channel,1);
  //控制VBAT引脚输出对应电压
  LIN_VBAT_Init(Channel,VBAT_12V);
}


void LIN_Config(uint8_t Channel,uint32_t BaudRate,uint8_t MasterMode,uint8_t VbatOut)
{
  LINConfig[Channel].InitFlag = FUNC_LIN;
  LIN_BUF_Init(&LINRxBuffer[Channel],(LIN_EX_MSG*)LINRxDataBuf[0],LIN_BUFFER_SIZE);
  LIN_BUF_Init(&LINTxBuffer[Channel],(LIN_EX_MSG*)LINTxDataBuf[0],LIN_BUFFER_SIZE);
  //关闭中断
  HAL_NVIC_DisableIRQ(lin_irq[Channel]);
  HAL_NVIC_DisableIRQ(lin_dmatx_irq[Channel]);
  //禁止DMA，可以解决UART运行之后，LIN无法正常收发的问题
  lin_dmatx_handle[Channel].Instance = lin_dmatx_stream[Channel];
  lin_dmarx_handle[Channel].Instance = lin_dmarx_stream[Channel];
  HAL_DMA_DeInit(&lin_dmatx_handle[Channel]);
  HAL_DMA_DeInit(&lin_dmarx_handle[Channel]);
  
  LINConfig[Channel].BaudRate = BaudRate;
  LINConfig[Channel].MasterMode = MasterMode;
  LINConfig[Channel].VbatOut = VbatOut;
  //PWM输出控制引脚初始化
  LIN_PWM_CTL_Init(Channel);
  //主从使能控制引脚初始化
  LIN_MEN_Init(Channel,MasterMode);
  //控制VBAT引脚输出对应电压
  LIN_VBAT_Init(Channel,VbatOut);
  //LIN串口初始化
  LIN_UART_Init(Channel,BaudRate);
  //LIN DMA发送数据配置
  LIN_DMA_TX_Init(Channel);
  //开启中断
  LIN_IT_Init(Channel);
  //debug_printf("LIN_IT_Init\r\n");
  //debug_printf("LIN_Init Ch=%d\r\n",Channel);
  //HAL_UART_Transmit_DMA(&lin_huart[Channel],"123456789",9);
}

void LIN_TimeProcess(void)
{
  for(int i=0;i<MAX_LIN_CH;i++){
    if(LINConfig[i].InitFlag){
      if(LIN_RxTimeOut[i]>0){
        LIN_RxTimeOut[i]--;
        if(LIN_RxTimeOut[i]==0){
          if(LinRxState[i] != IDLE){
            if(__HAL_UART_GET_FLAG(&lin_huart[i],UART_FLAG_IDLE)){
              LIN_EX_MSG *pRxMsg = LIN_BUF_GetRxingMsg(&LINRxBuffer[i]);
              if(pRxMsg->PID==0x3C){
                LIN_BUF_ApendMsg(&LINRxBuffer[i]);
              }
              LinRxState[i] = IDLE;
            }else{
              LIN_RxTimeOut[i] = 3*10000/LINConfig[i].BaudRate;//正在接收数据则继续延时
            }
          }
        }
      }
    }
  }
}


void LIN_EX_RxAsync(uint8_t Channel,uint8_t data)
{
  //debug_printf("S = %d\n",LinRxState[Channel]);
  //LIN_EX_MSG *pLastMsg = LIN_EX_GetLastMsg(Channel);
  LIN_EX_MSG *pRxMsg = LIN_BUF_GetRxingMsg(&LINRxBuffer[Channel]);
  switch(LinRxState[Channel]){
    case SYNCH:
      pRxMsg->Sync = data;
      //pRxMsg->MsgType = LIN_EX_MSG_TYPE_SY;
      LinRxState[Channel] = ID_LEN;
      break;
    case ID_LEN:
      pRxMsg->PID = data;
      //pRxMsg->MsgType = LIN_EX_MSG_TYPE_ID;
      //if(!LINMasterMode[Channel]){
      if((pRxMsg->Sync == 0x55)&&((pRxMsg->PID&0x3F)==0x3D)){
        LIN_EX_MSG *pMsg;
        if(LIN_BUF_GetRxedMsgWithSize(&LINTxBuffer[Channel],&pMsg,1)){
          HAL_StatusTypeDef ret = HAL_UART_Transmit_DMA(&lin_huart[Channel],pMsg->Data,pMsg->DataLen+1);
          //debug_printf("Tx data dma %d\n",ret);
        }
        //debug_printf("status = %d\r\n",status);
      }
      LinRxState[Channel] = DATA_GET;
      //}
      break;
    case DATA_GET:
      pRxMsg->Data[pRxMsg->DataLen] = data;
      pRxMsg->Check = data;
      pRxMsg->DataLen++;
      //pRxMsg->MsgType = LIN_EX_MSG_TYPE_DT;
      if(pRxMsg->DataLen >= 8){
        LinRxState[Channel] = CHECKSUM;
      }else{
        LinRxState[Channel] = DATA_GET;
      }
      break;
    case CHECKSUM:
      //__HAL_UART_DISABLE_IT(&lin_huart[Channel],UART_IT_IDLE);
      LIN_RxTimeOut[Channel] = -1;
      pRxMsg->Check = data;
      //pRxMsg->MsgType = LIN_EX_MSG_TYPE_CK;
      //完整接收到了当前帧，更新从机接收数据调度表
      //LIN_EX_UpdateSlaveScheduleTable(Channel,pRxMsg);
      //LinLastRxMsg[Channel] = *pRxMsg;
      if(pRxMsg->PID==0x3C){
        LIN_BUF_ApendMsg(&LINRxBuffer[Channel]);
      }
      LinRxState[Channel] = IDLE;
      /*debug_printf("get msg[%02X]:",pRxMsg->PID);
      for(int i=0;i<pRxMsg->DataLen;i++){
        debug_printf("%02X ",pRxMsg->Data[i]);
      }
      debug_printf("\n");*/
      break;
    default:
      break;
  }
}

void LIN_UART_IRQHandler(int Channel)
{
  uint32_t isrflags   = READ_REG(lin_huart[Channel].Instance->SR);
  uint32_t cr1its     = READ_REG(lin_huart[Channel].Instance->CR1);
  uint32_t cr3its     = READ_REG(lin_huart[Channel].Instance->CR3);
  uint32_t errorflags = 0x00U;
  uint32_t dmarequest = 0x00U;

  /* If no error occurs */
  errorflags = (isrflags & (uint32_t)(USART_SR_PE | USART_SR_FE | USART_SR_ORE | USART_SR_NE));
  if(errorflags){
    __HAL_UART_CLEAR_FEFLAG(&lin_huart[Channel]);//清除所有错误标志
  }
  LIN_EX_MSG *pRxMsg = LIN_BUF_GetRxingMsg(&LINRxBuffer[Channel]);
  //debug_printf("%08X %08X\r\n",cr1its,isrflags);
  //debug_printf("ISR\r\n");
  if(LINConfig[Channel].InitFlag==FUNC_LIN){
    //处理BREAK中断
    if((isrflags & USART_SR_LBD) != RESET){
      __HAL_UART_CLEAR_FLAG(&lin_huart[Channel], UART_FLAG_LBD);
      //__HAL_UART_DISABLE_IT(&lin_huart[Channel],UART_IT_IDLE);
      LIN_RxTimeOut[Channel] = 12*10000/LINConfig[Channel].BaudRate;
      //debug_printf("%d LBDF\r\n",Channel);
      if((LinRxState[Channel] == IDLE)||(LinRxState[Channel] == BREAK)){//上一帧数据被完整接收
        //LastGetMsg[Channel].MsgType = LIN_EX_MSG_TYPE_CK;
        LIN_BUF_ClearRxingMsg(&LINRxBuffer[Channel]);
        //pRxMsg->MsgType = LIN_EX_MSG_TYPE_BK;
        LinRxState[Channel] = SYNCH;
        //debug_printf("Get Break\n");
      }else{
        //LastGetMsg[Channel].MsgType = LIN_EX_MSG_TYPE_BK;
        LIN_BUF_ApendMsg(&LINRxBuffer[Channel]);
        LIN_BUF_ClearRxingMsg(&LINRxBuffer[Channel]);
        pRxMsg = LIN_BUF_GetRxingMsg(&LINRxBuffer[Channel]);
        //pRxMsg->MsgType = LIN_EX_MSG_TYPE_BK;
        LinRxState[Channel] = SYNCH;
      }
      //__HAL_UART_ENABLE_IT(&lin_huart[Channel],UART_IT_IDLE);
      //debug_printf("LS = %d\n",LinRxState[Channel]);
      //sendchar('B');
      return;
    }
    //处理接收中断
    if((isrflags & USART_SR_RXNE) != RESET){
      //必须加此判断，否则在正常接收数据的时候，若检测到了BREAK信号，那么上一帧数据会多接收到一个0x00数据
      if((isrflags&USART_SR_FE) == 0){
        uint8_t data = READ_REG(lin_uart[Channel]->DR);
        if((data==0x55)&&(LinRxState[Channel]==IDLE)){
          LIN_RxTimeOut[Channel] = 12*10000/LINConfig[Channel].BaudRate;
          LIN_BUF_ClearRxingMsg(&LINRxBuffer[Channel]);
          //pRxMsg->MsgType = LIN_EX_MSG_TYPE_BK;
          LinRxState[Channel] = SYNCH;
        }
        LIN_EX_RxAsync(Channel,data);
        //sendchar('D');
      }else{
        READ_REG(lin_uart[Channel]->DR);
        //sendchar('E');
      }
    }
    //处理空闲中断
    /*if((isrflags&USART_ISR_IDLE) != RESET){
      __HAL_UART_CLEAR_IT(&lin_huart[Channel], UART_CLEAR_IDLEF);
      if(LinRxState[Channel] != IDLE){
        LIN_EX_UpdateSlaveScheduleTable(Channel,pRxMsg);
        LinLastRxMsg[Channel] = *pRxMsg;
        LIN_BUF_ApendMsg(Channel);
        LinRxState[Channel] = IDLE;
      }
      //debug_printf("IS = %d\n",LinRxState[Channel]);
    }*/
  }else{
    lin_uart[Channel]->DR;
    __HAL_UART_CLEAR_FLAG(&lin_huart[Channel], UART_FLAG_LBD);
  }
  //处理发送完毕中断,必须加，否则DMA发送数据只能发送一次
  if((isrflags & USART_SR_TC) != RESET){
    CLEAR_BIT(lin_uart[Channel]->CR1, USART_CR1_TCIE);//必须加，否则启动DMA发送后会重复进入中断函数，导致程序异常
    HAL_UART_TxCpltCallback(&lin_huart[Channel]);
  }
}

uint8_t LIN_EX_GetCheckSum(uint8_t *pData,uint8_t len)
{
  uint16_t check_sum_temp=0;
  uint8_t i;
  for(i=0;i<len;i++){
    check_sum_temp += pData[i];
    if(check_sum_temp > 0xFF){
        check_sum_temp -= 0xFF;
    }
  }
  return (~check_sum_temp)&0xFF;
}

//只有在中断里面调用HAL_UART_IRQHandler函数才会进入该函数
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
  //debug_printf("UART DMA TX END\r\n");
  //debug_printf("HAL_UART_STATE = %d\r\n",huart->gState);
  huart->gState = HAL_UART_STATE_READY;
  //debug_printf("HAL_UART_STATE_READY\r\n");
  //huart->gState = HAL_UART_STATE_READY;
}
//void LIN_SendBreak(uint8_t Channel)
//{
//  HAL_LIN_SendBreak(&lin_huart[Channel]);
//  //等待BREAK发送完毕
//  while(__HAL_UART_GET_FLAG(&lin_huart[Channel],UART_FLAG_SBKF)==SET);
//}
HAL_StatusTypeDef LIN_UART_Transmit(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size)
{
//  while(Size--){
//    while(__HAL_UART_GET_FLAG(huart, UART_FLAG_TXE)==RESET);
//    huart->Instance->TDR = *pData++;
//  }
//  while(__HAL_UART_GET_FLAG(huart, UART_FLAG_TC)==RESET);
  //使用DMA发送，在多通道同时发送时，其效率更高
  HAL_UART_Transmit_DMA(huart,pData,Size);
  while(huart->gState != HAL_UART_STATE_READY){
    //osDelay(1);
  }
  return HAL_OK;
}

/**
  * @brief  This function handles UART interrupt request.
  * @param  None
  * @retval None
  * @Note   This function is redefined in "main.h" and related to DMA stream
  *         used for USART data transmission
  */
void USART1_IRQHandler(void)
{
  if(LINConfig[0].InitFlag==FUNC_LIN){
    LIN_UART_IRQHandler(0);
  }else{
    HAL_UART_IRQHandler(&lin_huart[0]);
//    __HAL_UART_CLEAR_IT(&lin_huart[0], 0xF);//清除所有错误标志
//    __HAL_UART_CLEAR_IT(&lin_huart[0], UART_CLEAR_LBDF);
  }
//  debug_printf("U1 IRQ\r\n");
}


void USART1_DMA_TX_IRQHandler(void)
{
  //lin_huart[0].gState = HAL_UART_STATE_READY;
  HAL_DMA_IRQHandler(&lin_dmatx_handle[0]);
  //debug_printf("USART1_DMA_TX_IRQHandler\r\n");
//  __HAL_DMA_CLEAR_FLAG(&lin_dmatx_handle[0], DMA_FLAG_TCIF1_5);
//  __HAL_UNLOCK(&lin_dmatx_handle[0]);
}


#endif
