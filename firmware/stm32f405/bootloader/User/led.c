#include "main.h"
#include "led.h"

void MX_LED_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct={0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin : PD11 */
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  
  GPIO_InitStruct.Pin = LED_RUN_PIN;
  HAL_GPIO_Init(LED_RUN_PORT, &GPIO_InitStruct);
  
  GPIO_InitStruct.Pin = LED_BOOT_PIN;
  HAL_GPIO_Init(LED_BOOT_PORT, &GPIO_InitStruct);
  
  GPIO_InitStruct.Pin = LED_BUSY1_PIN;
  HAL_GPIO_Init(LED_BUSY1_PORT, &GPIO_InitStruct);
  
  GPIO_InitStruct.Pin = LED_BUSY2_PIN;
  HAL_GPIO_Init(LED_BUSY2_PORT, &GPIO_InitStruct);
  
  LED_RUN_Off();
  LED_BOOT_Off();
  LED_BUSY1_Off();
  LED_BUSY2_Off();
}

void LED_RUN_On(void)
{
  HAL_GPIO_WritePin(LED_RUN_PORT, LED_RUN_PIN, GPIO_PIN_RESET);
}

void LED_RUN_Off(void)
{
  HAL_GPIO_WritePin(LED_RUN_PORT, LED_RUN_PIN, GPIO_PIN_SET);
}

void LED_BOOT_On(void)
{
  HAL_GPIO_WritePin(LED_BOOT_PORT, LED_BOOT_PIN, GPIO_PIN_RESET);
}

void LED_BOOT_Off(void)
{
  HAL_GPIO_WritePin(LED_BOOT_PORT, LED_BOOT_PIN, GPIO_PIN_SET);
}

void LED_BUSY1_On(void)
{
  HAL_GPIO_WritePin(LED_RUN_PORT, LED_BUSY1_PIN, GPIO_PIN_RESET);
}

void LED_BUSY1_Off(void)
{
  HAL_GPIO_WritePin(LED_RUN_PORT, LED_BUSY1_PIN, GPIO_PIN_SET);
}

void LED_BUSY2_On(void)
{
  HAL_GPIO_WritePin(LED_RUN_PORT, LED_BUSY2_PIN, GPIO_PIN_RESET);
}

void LED_BUSY2_Off(void)
{
  HAL_GPIO_WritePin(LED_RUN_PORT, LED_BUSY2_PIN, GPIO_PIN_SET);
}

void LED_LINBusyOn(uint8_t Ch)
{
  if(Ch < 2){
    LED_BUSY1_On();
  }else{
    LED_BUSY2_On();
  }
}

void LED_LINBusyOff(uint8_t Ch)
{
  if(Ch < 2){
    LED_BUSY1_Off();
  }else{
    LED_BUSY2_Off();
  }
}

void LED_CANBusyOn(uint8_t Ch)
{
  if(Ch < 1){
    LED_BUSY1_On();
  }else{
    LED_BUSY2_On();
  }
}

void LED_CANBusyOff(uint8_t Ch)
{
  if(Ch < 1){
    LED_BUSY1_Off();
  }else{
    LED_BUSY2_Off();
  }
}

