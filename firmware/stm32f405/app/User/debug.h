
#ifndef __DEBUG_H
#define __DEBUG_H

void MX_USART2_UART_Init(void);
void sendchar(unsigned char ch);
int debug_printf(const char *format, ...);
int debug_sprintf(char *out, const char *format, ...);

#endif
