/**
  ******************************************************************************
  * @file    offlin_function.c
  * $Author: wdluo $
  * $Revision: 229 $
  * $Date:: 2014-05-13 13:00:02 +0800 #$
  * @brief   全局定时器相关函数.
  ******************************************************************************
  * @attention
  *
  *<h3><center>&copy; Copyright 2009-2012, usbxyz</center>
  *<center><a href="http:\\www.toomoss.com">http://www.toomoss.com</a></center>
  *<center>All Rights Reserved</center></h3>
  * 
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <stdlib.h>
#include <string.h>
#include "lin_driver.h"

TIM_HandleTypeDef GlobalTimHandle;

void TIM7_IRQHandler(void)
{
  if(__HAL_TIM_GET_IT_SOURCE(&GlobalTimHandle,TIM_IT_UPDATE)){
    __HAL_TIM_CLEAR_IT(&GlobalTimHandle,TIM_IT_UPDATE);
    LIN_TimeProcess();
  }
}
/**
  * @brief  TIM定时器设置，用于CAN2LIN调度表定时调度
  * @param  None
  * @retval None
  */
void GLOBAL_TIM_Config(void)
{
  static uint8_t TimeConfigFlag=0;
  if(TimeConfigFlag){
    return;
  }
  __HAL_RCC_TIM7_CLK_ENABLE();
  
  /*##-2- Configure the NVIC for TIMx ########################################*/
  /* Set the TIMx priority */
  HAL_NVIC_SetPriority(TIM7_IRQn, 1, 1);

  /* Enable the TIMx global Interrupt */
  HAL_NVIC_EnableIRQ(TIM7_IRQn);
  /* Set TIMx instance */
  GlobalTimHandle.Instance = TIM7;

  /* Initialize TIMx peripheral as follows:
       + Period = 10000 - 1
       + Prescaler = (SystemCoreClock/10000) - 1
       + ClockDivision = 0
       + Counter direction = Up
  */
  GlobalTimHandle.Init.Period            = 10-1;//1ms一次中断
  GlobalTimHandle.Init.Prescaler         = (uint32_t)(SystemCoreClock / (2*10000)) - 1;
  GlobalTimHandle.Init.ClockDivision     = 0;
  GlobalTimHandle.Init.CounterMode       = TIM_COUNTERMODE_UP;
  GlobalTimHandle.Init.RepetitionCounter = 0;

  if(HAL_TIM_Base_Init(&GlobalTimHandle) != HAL_OK){
    /* Initialization Error */
    Error_Handler();
  }
  /*##-2- Start the TIM Base generation in interrupt mode ####################*/
  /* Start Channel1 */
  if(HAL_TIM_Base_Start_IT(&GlobalTimHandle) != HAL_OK){
    /* Starting Error */
    Error_Handler();
  }
  TimeConfigFlag = 1;
}

