#ifndef __LIN_DRIVER_H
#define __LIN_DRIVER_H

#define LIN1_MEN_PORT   GPIOD
#define LIN2_MEN_PORT   GPIOD
#define LIN3_MEN_PORT   GPIOD
#define LIN4_MEN_PORT   GPIOD

#define LIN1_MEN_PIN    GPIO_PIN_4
#define LIN2_MEN_PIN    GPIO_PIN_5
#define LIN3_MEN_PIN    GPIO_PIN_10
#define LIN4_MEN_PIN    GPIO_PIN_11

#define LIN1_VBAT12V_PORT  GPIOE
#define LIN2_VBAT12V_PORT  GPIOE
#define LIN3_VBAT12V_PORT  GPIOE
#define LIN4_VBAT12V_PORT  GPIOE

#define LIN1_VBAT5V_PORT  GPIOE
#define LIN2_VBAT5V_PORT  GPIOE
#define LIN3_VBAT5V_PORT  GPIOE
#define LIN4_VBAT5V_PORT  GPIOE

#define LIN1_VBAT12V_PIN  GPIO_PIN_12
#define LIN2_VBAT12V_PIN  GPIO_PIN_12
#define LIN3_VBAT12V_PIN  GPIO_PIN_13
#define LIN4_VBAT12V_PIN  GPIO_PIN_13

#define LIN1_VBAT5V_PIN  GPIO_PIN_14
#define LIN2_VBAT5V_PIN  GPIO_PIN_14
#define LIN3_VBAT5V_PIN  GPIO_PIN_15
#define LIN4_VBAT5V_PIN  GPIO_PIN_15

//LIN端口功能定义
#define FUNC_NONE     0
#define FUNC_LIN      1
#define FUNC_PWM      2
#define FUNC_BMM      3
#define FUNC_UART     4
#define FUNC_PWM_CAP  5
#define FUNC_FASTLIN  6

//VBAT输出电压定义
#define VBAT_0V     0
#define VBAT_12V    1
#define VBAT_5V     2

#define LIN_EX_CHECK_STD     0	//标准校验，不含PID
#define LIN_EX_CHECK_EXT     1	//增强校验，包含PID
#define LIN_EX_CHECK_USER    2	//自定义校验类型，需要用户自己计算并传入Check，不进行自动校验
#define LIN_EX_CHECK_NONE    3  //接收数据校验错误
#define LIN_EX_CHECK_ERROR   4  //接收数据校验错误

#define LIN_BREAK_BITS_10    0x00
#define LIN_BREAK_BITS_11    0x20

#define LIN_MASTER              1
#define LIN_SLAVE               0

#define	LIN_EX_MSG_TYPE_UN    0 //未知类型
#define	LIN_EX_MSG_TYPE_MW		1	//主机向从机发送数据
#define	LIN_EX_MSG_TYPE_MR		2	//主机从从机读取数据
#define	LIN_EX_MSG_TYPE_SW		3	//从机发送数据
#define	LIN_EX_MSG_TYPE_SR		4	//从机接收数据
#define	LIN_EX_MSG_TYPE_BK		5	//只发送BREAK信号，若是反馈回来的数据，表明只检测到BREAK信号
#define	LIN_EX_MSG_TYPE_SY		6	//表明检测到了BREAK，SYNC信号
#define	LIN_EX_MSG_TYPE_ID		7	//表明检测到了BREAK，SYNC，PID信号
#define	LIN_EX_MSG_TYPE_DT		8	//表明检测到了BREAK，SYNC，PID,DATA信号
#define	LIN_EX_MSG_TYPE_CK		9	//表明检测到了BREAK，SYNC，PID,DATA,CHECK信号

//定义从机操作模式
#define LIN_SLAVE_WRITE     0
#define LIN_SLAVE_READ      1
//LIN数据处理状态定义
typedef enum{ 
  IDLE=0, 
  BREAK,
  SYNCH,
  ID_LEN, 
  DATA_GET,
  CHECKSUM
}LIN_STATE;
//LIN消息定义
typedef struct _LIN_MSG{
  unsigned char CheckType;//校验类型
  unsigned char DataLen;	//LIN数据段有效数据字节数
  unsigned char Sync;			//固定值，0x55
  unsigned char PID;			//帧ID		
  unsigned char Data[8];	//数据
  unsigned char Check;		//校验,只有校验数据类型为LIN_EX_CHECK_USER的时候才需要用户传入数据
}LIN_EX_MSG,*PLIN_EX_MSG;
//LIN调度表定义
typedef struct _LIN_SCH{
  uint8_t   RunFlag;
  uint8_t   MsgIndex;
  uint16_t  AllMsgLen;
  void      *pMsg;
}LIN_SCH;

//定义初始化LIN初始化数据类型
typedef struct _LIN_CONFIG{
  uint32_t BaudRate;
  uint8_t  InitFlag;
  uint8_t  MasterMode;
  uint8_t  VbatOut;
  uint8_t  BreakBits;
}LIN_CONFIG;

//PID计算
#define GET_PID(data) ((data&0x3F)|((((data&0x01)^((data>>1)&0x01)^((data>>2)&0x01)^((data>>4)&0x01))&0x01)<<6)|(((~(((data>>1)&0x01)^((data>>3)&0x01)^((data>>4)&0x01)^((data>>5)&0x01)))&0x01)<<7))

//函数定义
void LIN_MEN_Init(int Channel,uint8_t Master);
void LIN_VBAT_PowerOut(int Channel,uint8_t out_power);
void LIN_VBAT_Init(int Channel,uint8_t out_power);
void LIN_PWM_CTL_Init(int Channel);
void LIN_DMA_TX_Init(uint8_t Channel);
void LIN_DMA_RX_Init(uint8_t Channel);
void LIN_UART_Init(int Channel,int BaudRate);
void LIN_IT_Init(uint8_t Channel);
int LIN_UART_ReceiveDataAsync(uint8_t Channel,uint8_t **pReadData);
void LIN_Config(uint8_t Channel,uint32_t BaudRate,uint8_t MasterMode,uint8_t VbatOut);
void LIN_SlaveSetIDMode(uint8_t Channel,LIN_EX_MSG *pInMsg,int MsgLen);
LIN_EX_MSG *LIN_SlaveGetIDMode(uint8_t Channel);
void LIN_MasterSetSchTable(uint8_t Channel,LIN_EX_MSG *pInMsg,int MsgLen);
void LIN_SchTimerConfig(uint8_t Channel);
void LIN_SchTimerStart(uint8_t Channel,int TimeoutMs);
void LINTaskFunction(void * argument);
void LIN_EX_MasterSync(uint8_t Channel,LIN_EX_MSG *pInMsg,int MsgLen);
uint8_t LIN_EX_GetCheckSum(uint8_t *pData,uint8_t len);
void LIN_HardwareConfig(uint8_t Channel);
void LIN_TimeProcess(void);
#endif

