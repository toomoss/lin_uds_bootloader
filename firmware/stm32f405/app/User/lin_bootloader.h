#ifndef __LIN_BOOTLOADER_H
#define __LIN_BOOTLOADER_H

#define BOOT_START_ADDR         0x08000000  //BOOT程序起始地址
#define APP_START_ADDR          0x08008000  //APP程序起始地址
#define APP_VALID_FLAG_ADDR     0x08004000  //APP程序有效标志存储地址
#define BOOT_REQ_FLAG_ADDR      0x08004010  //刷新请求标志存储地址
//标志定义
#define APP_VALID_FLAG  0x55AA55AA
#define BOOT_REQ_FLAG   0xAA55AA55
//固件类型值定义
#define FW_TYPE_BOOT     0x55
#define FW_TYPE_APP      0xAA
//定义当前固件类型为BOOT
#define FW_TYPE         FW_TYPE_BOOT

uint8_t LIN_BOOT_GetProgRequest(void);
void LIN_BOOT_SetProgRequest(void);
void LIN_BOOT_ResetProgRequest(void);
void LIN_BOOT_Reset(void);
void LIN_BOOT_ExeApp(void);
uint8_t LIN_BOOT_EraseApp(void);
uint8_t LIN_BOOT_CheckApp(void);
uint8_t LIN_BOOT_WriteDataToFlash(uint32_t Addr,uint8_t *pData,uint32_t DataLen);
void LIN_BOOT_Response(uint8_t RSID,uint8_t *pData,uint16_t DataLen);
void LIN_BOOT_ProcessCmd(uint8_t SID,uint8_t *pData,uint16_t DataLen);


#endif

