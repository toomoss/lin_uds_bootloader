#include "main.h"
#include "debug.h"
#include "led.h"
#include "lin_driver.h"
#include "lin_buffer.h"
#include "lin_uds.h"
#include "crc32.h"
#include "lin_bootloader.h"

typedef  void (*pFunction)(void);

/* Base address of the Flash sectors */
#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */
#define ADDR_FLASH_SECTOR_8     ((uint32_t)0x08080000) /* Base @ of Sector 8, 128 Kbytes */
#define ADDR_FLASH_SECTOR_9     ((uint32_t)0x080A0000) /* Base @ of Sector 9, 128 Kbytes */
#define ADDR_FLASH_SECTOR_10    ((uint32_t)0x080C0000) /* Base @ of Sector 10, 128 Kbytes */
#define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000) /* Base @ of Sector 11, 128 Kbytes */ 

/**
  * @brief  Gets the sector of a given address
  * @param  None
  * @retval The sector of a given address
  */
uint32_t GetSector(uint32_t Address)
{
  uint32_t sector = 0;
  
  if((Address < ADDR_FLASH_SECTOR_1) && (Address >= ADDR_FLASH_SECTOR_0))
  {
    sector = FLASH_SECTOR_0;  
  }
  else if((Address < ADDR_FLASH_SECTOR_2) && (Address >= ADDR_FLASH_SECTOR_1))
  {
    sector = FLASH_SECTOR_1;  
  }
  else if((Address < ADDR_FLASH_SECTOR_3) && (Address >= ADDR_FLASH_SECTOR_2))
  {
    sector = FLASH_SECTOR_2;  
  }
  else if((Address < ADDR_FLASH_SECTOR_4) && (Address >= ADDR_FLASH_SECTOR_3))
  {
    sector = FLASH_SECTOR_3;  
  }
  else if((Address < ADDR_FLASH_SECTOR_5) && (Address >= ADDR_FLASH_SECTOR_4))
  {
    sector = FLASH_SECTOR_4;  
  }
  else if((Address < ADDR_FLASH_SECTOR_6) && (Address >= ADDR_FLASH_SECTOR_5))
  {
    sector = FLASH_SECTOR_5;  
  }
  else if((Address < ADDR_FLASH_SECTOR_7) && (Address >= ADDR_FLASH_SECTOR_6))
  {
    sector = FLASH_SECTOR_6;  
  }
  else if((Address < ADDR_FLASH_SECTOR_8) && (Address >= ADDR_FLASH_SECTOR_7))
  {
    sector = FLASH_SECTOR_7;  
  }
  else if((Address < ADDR_FLASH_SECTOR_9) && (Address >= ADDR_FLASH_SECTOR_8))
  {
    sector = FLASH_SECTOR_8;  
  }
  else if((Address < ADDR_FLASH_SECTOR_10) && (Address >= ADDR_FLASH_SECTOR_9))
  {
    sector = FLASH_SECTOR_9;  
  }
  else if((Address < ADDR_FLASH_SECTOR_11) && (Address >= ADDR_FLASH_SECTOR_10))
  {
    sector = FLASH_SECTOR_10;  
  }
  else/*(Address < FLASH_END_ADDR) && (Address >= ADDR_FLASH_SECTOR_11))*/
  {
    sector = FLASH_SECTOR_11;  
  }

  return sector;
}

uint8_t LIN_BOOT_GetProgRequest(void)
{
  //读取请求固件升级标志，需要自己实现
  if(*((uint32_t *)BOOT_REQ_FLAG_ADDR)==BOOT_REQ_FLAG){
    return 1;
  }else{
    return 0;
  }
}
//将固件升级标志置位
void LIN_BOOT_SetProgRequest(void)
{
  __set_PRIMASK(1);//关闭中断
  HAL_FLASH_Unlock();//解锁
  FLASH_Erase_Sector(GetSector(ADDR_FLASH_SECTOR_1),FLASH_VOLTAGE_RANGE_3);
  HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,BOOT_REQ_FLAG_ADDR,BOOT_REQ_FLAG);
  HAL_FLASH_Lock();//上锁
  __set_PRIMASK(0);//开启中断
}

//将固件升级标志复位
void LIN_BOOT_ResetProgRequest(void)
{
  uint32_t AppValidFlag = *((uint32_t *)APP_VALID_FLAG_ADDR);
  __set_PRIMASK(1);//关闭中断
  HAL_FLASH_Unlock();//解锁
  FLASH_Erase_Sector(GetSector(ADDR_FLASH_SECTOR_1),FLASH_VOLTAGE_RANGE_3);
  HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,APP_VALID_FLAG_ADDR,AppValidFlag);
  HAL_FLASH_Lock();//上锁
  __set_PRIMASK(0);//开启中断
}

/**
 * @brief  将固件升级标志置位
 */
void LIN_BOOT_SetAppValid(void)
{ 
  __set_PRIMASK(1);//关闭中断
  HAL_FLASH_Unlock();//解锁
  FLASH_Erase_Sector(GetSector(ADDR_FLASH_SECTOR_1),FLASH_VOLTAGE_RANGE_3);
  HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,APP_VALID_FLAG_ADDR,APP_VALID_FLAG);
  HAL_FLASH_Lock();//上锁
  __set_PRIMASK(0);//开启中断
}

//软件复位程序
void LIN_BOOT_Reset(void)
{
  __set_PRIMASK(1);//关闭中断
  NVIC_SystemReset();
}
//执行APP程序
void LIN_BOOT_ExeApp(void)
{
  pFunction JumpToApp;
  __IO uint32_t JumpAddress; 
  __set_PRIMASK(1);//关闭全局中断，注意，必须在APP中打开全局中断，否则可能会导致APP中断程序不正常
  if (((*(__IO uint32_t*)APP_START_ADDR) & 0x2FFE0000 ) == 0x20000000)
  { 
    JumpAddress = *(__IO uint32_t*) (APP_START_ADDR + 4);
    JumpToApp = (pFunction) JumpAddress;
    __set_MSP(*(__IO uint32_t*)APP_START_ADDR);
    JumpToApp();
  }
}
//擦除应用程序，返回1-失败，0-成功
uint8_t LIN_BOOT_EraseApp(void)
{
  uint32_t StartSector, EndSector;
  uint32_t SectorCounter=0;
  __set_PRIMASK(1);//关闭中断
  HAL_FLASH_Unlock();//解锁
  /* Get the number of the start and end sectors */
  StartSector = GetSector(ADDR_FLASH_SECTOR_2);
  EndSector = GetSector(ADDR_FLASH_SECTOR_6);
  for(SectorCounter = StartSector; SectorCounter <= EndSector; SectorCounter += 8){
    FLASH_Erase_Sector(SectorCounter, FLASH_VOLTAGE_RANGE_3);
  }
  HAL_FLASH_Lock();//上锁
  __set_PRIMASK(0);//开启中断
  return 0;
}
//判断APP是否有效
uint8_t LIN_BOOT_CheckApp(void)
{
  return 0;
}

//将数据写入Flash
uint8_t LIN_BOOT_WriteDataToFlash(uint32_t StartAddr,uint8_t *pData,uint32_t DataLen)
{
  HAL_StatusTypeDef FLASHStatus;

  uint32_t *pDataTemp=(uint32_t *)pData;
  uint32_t i;
  __set_PRIMASK(1);//关闭中断
  HAL_FLASH_Unlock();//解锁
  if(StartAddr<APP_VALID_FLAG_ADDR){
    return 2;
  }
  for(i=0;i<(DataLen>>2);i++)
  {
    FLASHStatus = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,StartAddr, *pDataTemp);
    if (FLASHStatus == HAL_OK){
      StartAddr += 4;
      pDataTemp++;
    }else{ 
      HAL_FLASH_Lock();//上锁
      __set_PRIMASK(0);//开启中断
      return 1;
    }
  }
  HAL_FLASH_Lock();//上锁
  __set_PRIMASK(0);//开启中断
  return	0;
}

void LIN_BOOT_Response(uint8_t RSID,uint8_t *pData,uint16_t DataLen)
{
  LIN_UDS_Respond(RSID,pData,DataLen);
}

void LIN_BOOT_ProcessCmd(uint8_t SID,uint8_t *pData,uint16_t DataLen)
{
  static uint32_t MemoryAddress;
  static uint32_t MemorySize;
  static uint32_t AppCRC32=0xFFFFFFFF;
  static uint8_t FirstCRC=1;
  const uint32_t BlockSize = UDS_PACK_SIZE+2;//Max Number Of Block Length
  static uint8_t BSC=1;//Block Sequence Counter
  switch(SID)
  {
    case 0x10:
      if((DataLen>=1)&&(pData[0]==0x02)){//Programming Mode
        AppCRC32=0xFFFFFFFF;
        FirstCRC = 1;
        debug_printf("Programming Mode\r\n");
        LIN_BOOT_Response(SID|0x40,pData,1);
        LIN_BOOT_SetProgRequest();
        HAL_Delay(100);//不延时上位机收不到响应
        LIN_BOOT_Reset();
      }
      break;
    case 0x11:
      if((DataLen>=1)&&(pData[0]==0x01)){//ECU Reset
        debug_printf("ECU Reset\r\n");
        LIN_BOOT_Response(SID|0x40,pData,1);
        HAL_Delay(100);//不延时上位机收不到响应
        LIN_BOOT_Reset();
      }
      break;
    case 0x31:
      if((DataLen>=3)&&(pData[0]==0x01)&&(pData[1]==0xFF)&&(pData[2]==0x00)){//Erase Flash Memory
        debug_printf("Erase Flash Memory\r\n");
        uint8_t res[2]={SID,0x78};//Request Correctly Received Response Pending
        LIN_BOOT_Response(0x7F,res,2);
        pData[3] = LIN_BOOT_EraseApp();
        LIN_BOOT_Response(SID|0x40,pData,4);
      }else if((DataLen>=3)&&(pData[0]==0x01)&&(pData[1]==0xFF)&&(pData[2]==0x01)){//Check Programming Dependencies,验证APP的合法性
        pData[3] = LIN_BOOT_CheckApp();
        LIN_BOOT_Response(SID|0x40,pData,4);
        debug_printf("Check Programming Dependencies res=%d\r\n",pData[3]);
      }else if((DataLen>=7)&&(pData[0]==0x01)&&(pData[1]==0xF0)&&(pData[2]==0x01)){//Check Programming Integrity,通过CRC32验证
        uint32_t CheckSum = (pData[3]<<24)|(pData[4]<<16)|(pData[5]<<8)|pData[6];
        if(AppCRC32==CheckSum){
          LIN_BOOT_SetAppValid();
          pData[3] = 0;
        }else{
          pData[3] = 1;
        }
        LIN_BOOT_Response(SID|0x40,pData,4);
        debug_printf("Check Programming Integrity res = %d GetCRC=%08X AppCRC=%08X\r\n",pData[3],CheckSum,AppCRC32);
      }else{
        pData[0] = SID;
        pData[1] = 0x73;//Wrong Block Sequence Counter
        LIN_BOOT_Response(0x7F,pData,2);
      }
      break;
    case 0x34:
      if((DataLen>=10)&&(pData[0]==0x00)&&(pData[1]==0x44)){//Request Download
        BSC = 1;
        MemoryAddress = (pData[2]<<24)|(pData[3]<<16)|(pData[4]<<8)|pData[5];
        MemorySize = (pData[6]<<24)|(pData[7]<<16)|(pData[8]<<8)|pData[9];
        debug_printf("Request Download MemoryAddress = 0x%08X MemorySize = %d Byte\r\n",MemoryAddress,MemorySize);
        pData[0] = 0x40;
        pData[1] = BlockSize>>24;
        pData[2] = BlockSize>>16;
        pData[3] = BlockSize>>8;
        pData[4] = BlockSize;
        LIN_BOOT_Response(SID|0x40,pData,5);
      }else if((DataLen>=6)&&(pData[0]==0x00)&&(pData[1]==0x22)){//Request Download
        BSC = 1;
        MemoryAddress = (pData[2]<<8)|(pData[3]<<0);
        MemorySize = (pData[4]<<8)|(pData[5]<<0);
        debug_printf("Request Download MemoryAddress = 0x%08X MemorySize = %d Byte\r\n",MemoryAddress,MemorySize);
        pData[0] = 0x20;
        pData[1] = BlockSize>>8;
        pData[2] = BlockSize;
        LIN_BOOT_Response(SID|0x40,pData,3);
      }else{
        pData[0] = SID;
        pData[1] = 0x13;
        LIN_BOOT_Response(0x7F,pData,2);
      }
      break;
    case 0x36:
      if(DataLen>=2){//Transfer Data
        debug_printf("Transfer Data BSC=%d\r\n",BSC);
        if(pData[0]==BSC){
          uint8_t res[2]={SID,0x78};//Request Correctly Received Response Pending
          LIN_BOOT_Response(0x7F,res,2);
          uint16_t DataNum = DataLen-1;
          uint8_t *pGotData = &pData[1];
          uint32_t AddrOffset = (BSC-1)*UDS_PACK_SIZE;
          LIN_BOOT_WriteDataToFlash(MemoryAddress+AddrOffset,pGotData,DataNum);
          if(FirstCRC){
            AppCRC32 = crc32(AppCRC32,(const uint8_t *)(MemoryAddress+AddrOffset),DataNum);
            FirstCRC = 0;
          }else{
            AppCRC32 = crc32(AppCRC32^0xFFFFFFFF,(const uint8_t *)(MemoryAddress+AddrOffset),DataNum);
          }
          LIN_BOOT_Response(SID|0x40,pData,1);
          BSC++;
          debug_printf("Write Data To Flash Addr=%08X\r\n",MemoryAddress+AddrOffset);
        }else{
          pData[0] = SID;
          pData[1] = 0x73;//Wrong Block Sequence Counter
          LIN_BOOT_Response(0x7F,pData,2);
        }
      }else{
        pData[0] = SID;
        pData[1] = 0x13;//Incorrect Message Length Or Invalid Format
        LIN_BOOT_Response(0x7F,pData,2);
      }
      break;
    case 0x37://Request Transfer Exit
      debug_printf("Request Transfer Exit\r\n");
      LIN_BOOT_Response(SID|0x40,pData,0);
      break;
    default:
      break;
  }
}

