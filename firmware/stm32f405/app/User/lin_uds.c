#include "main.h"
#include "lin_buffer.h"
#include "lin_bootloader.h"
#include <string.h>

extern LIN_BUFFER    LINTxBuffer[MAX_LIN_CH];

#define LIN_UDS_OK            0
#define LIN_UDS_TRAN_USB      -98
#define LIN_UDS_TRAN_LIN      -99
#define LIN_UDS_TIMEOUT_A     -100
#define LIN_UDS_TIMEOUT_Bs    -101
#define LIN_UDS_TIMEOUT_Cr    -102
#define LIN_UDS_WRONG_SN      -103
#define LIN_UDS_INVALID_FS    -104
#define LIN_UDS_UNEXP_PDU     -105
#define LIN_UDS_WFT_OVRN      -106
#define LIN_UDS_BUFFER_OVFLW  -107
#define LIN_UDS_ERROR         -108

uint8_t LIN_UDS_GetNAD(void)
{
  return 0X0E;
}

void LIN_UDS_GotError(int ErrorCode)
{
}

void LIN_UDS_SendMsg(uint8_t *pData,uint16_t DataLen)
{
  LIN_EX_MSG *pLINTxMsg = LIN_BUF_GetRxingMsg(&LINTxBuffer[0]);
  pLINTxMsg->DataLen = 8;
  memset(pLINTxMsg->Data,0xFF,8);
  pLINTxMsg->Data[0] = LIN_UDS_GetNAD();
  for(int k=0;k<DataLen;k++){
    pLINTxMsg->Data[1+k] = pData[k];
  }
  pLINTxMsg->Check = LIN_EX_GetCheckSum(pLINTxMsg->Data,8);
  LIN_BUF_ApendMsg(&LINTxBuffer[0]);
}

void LIN_UDS_Respond(uint8_t RSID,uint8_t *pData,uint16_t DataLen)
{
  const int MaxSFDataLen = 6;
  const int MaxFFDataLen = 5;
  const int MaxCFDataLen = 6;
  if(DataLen>0xFFF){
      DataLen = 0xFFF;
  }
  int MaxSendDataSize = DataLen+1;
  uint8_t buffer[8];
  int DataIndex=0;
  if(MaxSendDataSize <= MaxSFDataLen){
      //SF单帧
      buffer[DataIndex++] = 0x0F&(MaxSendDataSize);//PCI
      buffer[DataIndex++] = RSID;
      for(int i=0;i<(MaxSendDataSize-1);i++){
          buffer[DataIndex++] = *pData++;
      }
      LIN_UDS_SendMsg(buffer,DataIndex);
  }else{
      //FF首帧
      buffer[DataIndex++] = 0x10|((MaxSendDataSize>>8)&0xF);
      buffer[DataIndex++] = MaxSendDataSize&0xFF;
      buffer[DataIndex++] = RSID;
      for(int i=0;i<(MaxFFDataLen-1);i++){
          buffer[DataIndex++] = *pData++;
      }
      LIN_UDS_SendMsg(buffer,DataIndex);
      //CF续帧
      int di=0;
      uint8_t CF_SN = 1;
      MaxSendDataSize -= MaxFFDataLen;//减去FF帧发送的5字节数据
      for(di=0;di<MaxSendDataSize;){
          int SendDataNum = ((di+MaxCFDataLen)>MaxSendDataSize)?(MaxSendDataSize%MaxCFDataLen):MaxCFDataLen;
          DataIndex = 0;
          buffer[DataIndex++] = 0x20|((CF_SN)&0xF);
          for(int i=0;i<SendDataNum;i++){
              buffer[DataIndex++] = *pData++;
          }
          LIN_UDS_SendMsg(buffer,DataIndex);
          CF_SN++;
          di += SendDataNum;
      }
  }
}

void LIN_UDS_GotRequest(uint8_t SID,uint8_t *pData,uint16_t DataLen)
{
  LIN_BOOT_ProcessCmd(SID,pData,DataLen);
#if 0
  debug_printf("LIN UDS REQ:");
  for(int j=0;j<DataLen;j++){
    debug_printf("%02X ",pData[j]);
  }
  debug_printf("\n");
#endif
}

void LIN_UDS_Process(LIN_BUFFER *pLINRxBuf)
{
  const int MaxSFDataLen = 6;
  const int MaxFFDataLen = 5;
  const int MaxCFDataLen = 6;
  static int AllDataLen = 0; //待结算数据总数
  static int GotDataNum = 0;
  static uint8_t CF_SN = 0;
  static uint32_t buffer[UDS_PACK_SIZE/4+2];
  uint8_t *pBuffer = (uint8_t *)(&buffer[0])+2;
  LIN_EX_MSG *pLINMsg;
  int MsgNum = LIN_BUF_GetAllRxedMsg(pLINRxBuf,&pLINMsg);
  for(int i=0;i<MsgNum;i++){
    //添加一帧发送帧，用于测试
    #if 0
    LIN_EX_MSG *pLINTxMsg = LIN_BUF_GetRxingMsg(&LINTxBuffer[0]);
    pLINTxMsg->DataLen = 8;
    pLINTxMsg->Data[0] = 0x01;
    pLINTxMsg->Data[1] = 0x06;
    pLINTxMsg->Data[2] = pLINMsg->Data[2]|0x40;
    for(int k=0;k<5;k++){
      pLINTxMsg->Data[3+k] = pLINMsg->Data[3+k];
    }
    pLINTxMsg->Check = LIN_EX_GetCheckSum(pLINTxMsg->Data,8);
    LIN_BUF_ApendMsg(&LINTxBuffer[0]);
    #endif
    //显示接收到的数据
#if 0 
    debug_printf("LIN RX MSG[%02X]:",pLINMsg[i].PID);
    for(int j=0;j<pLINMsg[i].DataLen;j++){
      debug_printf("%02X ",pLINMsg[i].Data[j]);
    }
    debug_printf("\r\n");
#endif
    //校验数据
    uint8_t check = LIN_EX_GetCheckSum(pLINMsg[i].Data,pLINMsg[i].DataLen);
    if((check == pLINMsg[i].Check)&&(pLINMsg[i].PID==0x3C)){
      if((LIN_UDS_GetNAD()==pLINMsg[i].Data[0])||(pLINMsg[i].Data[0]==0x7F)){//判断NAD
        int DataIndex = 0;
        uint8_t *buffer = &pLINMsg[i].Data[1];
        uint8_t PCI = buffer[DataIndex++];
        uint8_t PCIType = (PCI>>4)&0xF;
        switch(PCIType){
        case 0://SF
            AllDataLen = PCI&0xF;
            if(AllDataLen > MaxSFDataLen){
              LIN_UDS_GotError(LIN_UDS_UNEXP_PDU);
            }else if(AllDataLen > 0){
                for(int i=0;i<AllDataLen;i++){
                    pBuffer[i] = buffer[DataIndex++];
                }
                LIN_UDS_GotRequest(pBuffer[0],&pBuffer[1],AllDataLen-1);
            }
            break;
        case 1://FF
            AllDataLen = ((PCI&0xF)<<8)|buffer[DataIndex++];
            if(AllDataLen > MaxFFDataLen){
              for(int i=0;i<MaxFFDataLen;i++){
                  pBuffer[i] = buffer[DataIndex++];
              }
              CF_SN = 1;
              GotDataNum = MaxFFDataLen;
            }
            break;
        case 2://CF
            if((PCI&0xF)==(CF_SN&0xF)){
                for(int i=0;(i<MaxCFDataLen)&&(GotDataNum<AllDataLen);i++,GotDataNum++){
                    pBuffer[GotDataNum] = buffer[DataIndex++];
                }
                //printf("CF GotDataNum = %d\n",GotDataNum);
                CF_SN++;
                if((GotDataNum>=AllDataLen)&&(AllDataLen > 0)){
                  LIN_UDS_GotRequest(pBuffer[0],&pBuffer[1],AllDataLen-1);
                }
            }else{
              LIN_UDS_GotError(LIN_UDS_WRONG_SN);
            }
            break;
        default:
            LIN_UDS_GotError(LIN_UDS_UNEXP_PDU);
            break;
        }
      }
    }
  }
}

